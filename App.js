import React, { useState } from "react";
import { Button, Text, View } from "react-native";

const Cat = (props) => {
    const [isHungry, setIsHungry] = useState(true);

    return (
        <View>
            <Text>
                I am {props.name}, and I am {isHungry ? "hungry" : "full"}!
            </Text>
            <Button
                onPress={() => {
                    setIsHungry(false);
                }}
                // disabled={!isHungry}
                title={isHungry ? "Pour me some milk, please!" : "Thank you!"}
            />
        </View>
    );
}
const Cafe = () => {
    return (
        <>
            <Cat name="Munkustrap" />
            <Cat name="Spot" />
        </>
    );
}

export default Cafe;

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Hello!! Open up App.js to start working on your app!</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }
//
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
